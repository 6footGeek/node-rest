import dotenv from "dotenv";
import cors from "cors";
import express from "express";
dotenv.config();
console.log("Node is up");

const app = express();

app.use(cors());

app.get("/", (req, res) => {
  res.send("Recieved a GET request");
});

app.post("/", (req, res) => {
  res.send("Recieved a POST request");
});

app.put("/", (req, res) => {
  res.send("Recieved a PUT request");
});

app.delete("/", (req, res) => {
  res.send("Recieved a DELETE request");
});

//testing future design - true rest
app.get("/matters", (req, res) => {
  res.send("get all matters");
});

app.get("/projects", (req, res) => {
  res.send("get all projects");
});

app.get("/tasks", (req, res) => {
  res.send("get all tasks");
});

app.get("/tasks/:taskId", (req, res) => {
  res.send(`Got task ${req.params.taskId}`);
});

app.listen(process.env.PORT, () => {
  console.log(`EXPRESS IS UP ON PORT ${process.env.PORT}`);
});
